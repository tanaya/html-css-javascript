// script.js
let tasks = [];

function addTask() {
    const taskInput = document.getElementById("taskInput");
    const taskTitle = taskInput.value.trim();

    if (taskTitle !== "") {
        tasks.push({ title: taskTitle, completed: false });
        
        taskInput.value = "";
    }
}

function deleteTask(index) {
    tasks.splice(index, 1);
    renderTasks();
}

function toggleCompleted(index) {
    tasks[index].completed = !tasks[index].completed;
    renderTasks();
}

function filterTasks() {
    const filterSelect = document.getElementById("filterSelect");
    const filterValue = filterSelect.value;

    const filteredTasks = tasks.filter(task => {
        if (filterValue === "completed") {
            return task.completed;
        } else if (filterValue === "incomplete") {
            return !task.completed;
        } else {
            return true;
        }
    });

    renderTasks(filteredTasks);
}
function showAllTasks() {
    renderTasks(tasks);
}

function renderTasks(filteredTasks = tasks) {
    const taskList = document.getElementById("taskList");
    taskList.innerHTML = "";

    filteredTasks.forEach((task, index) => {
        const li = document.createElement("li");
        li.innerHTML = `
            <span class="${task.completed ? 'completed' : ''}">${task.title}</span>
            <div>
                <button onclick="toggleCompleted(${index})">${task.completed ? 'Undo' : 'Done'}</button>
                <button onclick="deleteTask(${index})">Delete</button>
            </div>
        `;
        taskList.appendChild(li);
    });
}
